# Because of Hope

[becauseofhope.org](http://www.becauseofhope.org) _Not yet deployed!_
[becauseofhope.org](http://alpha.becauseofhope.org) Testing

This is the sourcecode for Because of Hope's website. BOH is a 501(c)(3)
organization in Santa Barbara that helps widows and orphans through sustainable
means.

The website and infrastructure is still under heavy development and iteration.

We're replacing:

* $20/month VPS, GoDaddy registar/DNS, 2 HTML files

with

* 3¢/month Amazon S3 Static Website hosting, Cloudflare, Namecheap

This new setup will allow BOH to take on multiple DDOS attacks from the
internets and get away with paying for it all with the change we find in the
couch or nothing since Amazon waives all bills with less than 12¢ in them.

For now, all artwork, text, and design that made by Because of Hope should be
considered "All Rights Reserved". Everything else is under their original
license or BSD. You are more than welcome to use them as reference for your own
nanoc-powered site.

## Tools used

* Zurb Foundation
* Compass
* nanoc
* haml

## Expected Image Specifications

### Photoshop

"Save for Web", High, JPG, Progressive


### Header Images

Header images should have dimensions of 1000x200.


## References, notes, and links for stuff used.

[Credentials for fog deployment](https://github.com/ddfreyne/nanoc/issues/100)
