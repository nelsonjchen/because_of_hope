# Thank you for your interest in BOH!

We are always looking to add people to our team!  There are many ways you can get involved and join us in our effort to sustainably empower Ugandans.

**Become an Intern**: BOH has various internships throughout the year that enable people to be a part of our operations and volunteer their time and talents to further our mission.  Click the          tab to learn what internships are currently available and how to apply.

**Host a Fundraiser**: As part of our Beads Project, we sell Ugandan handmade jewelry and crafts at community and
school
 events and home parties.  This is a great way you can help raise awareness and funds for our cause.  Email us at info@becauseofhope.org if you are interested in hosting a fundraiser in your home or school.

**Become a Monthly Donor**: No matter the amount, you are able to make a significant impact in the lives of the people
in our programs.  A dollar really does go a long way, and on average a woman living in the villages we work with can only earn half of what is needed to support her family.  Any and every donation helps us in our goal of empowering these women and their children in Uganda.

**Sponsor a Student**:  Help a child go to school in Uganda through BOH's Student Scholarship Program (BSSP).  You are
able to sponsor a specific student or make a general donation to the program by clicking on       the tab.  Education is an essential step to breaking the cycle of poverty in a family, and you can be a part of making this possible in a student’s life.

**Help Raise Awareness**:  Advocating for Ugandan widows and orphans begins in your own backyard.  Whether talking with family and friends or using your social media network to share about BOH, you can make a huge impactthrough raising awareness of our cause.  Invite friends to "Like" our Facebook page or purchase a Media Kit online to help equip you to share BOH with your community.  For more ideas on how to advocate in your community, email us at info@becauseofhope.org.

**Pray for us**:  We believe in the power of prayer and it has been an integral part of BOH's operations because we
know that in and of ourselves, we are incapable of achieving our mission to sustainably empower widows and orphans. This mission often seems like a dream, too big to be possible, when we are faced with the mountains of poverty which have no simple route to eradication.  And yet, we have seen tremendous progress be made in our efforts to achieve this dream by being prayerful and empowered by Christ.Pray for BOH to operate in wisdom, unconditional love, and selfless compassion.  Regardless of your faith, we ask you to join us in our dream of sustainable development and empowerment in Uganda.
